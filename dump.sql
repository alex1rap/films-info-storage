DROP TABLE IF EXISTS films;

CREATE TABLE films
(
    id           INT         NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title        VARCHAR(64) NOT NULL,
    release_year INT         NOT NULL DEFAULT 1900,
    format       VARCHAR(12) NOT NULL DEFAULT '',
    stars        TEXT,
    UNIQUE (title, release_year)
);

DROP TABLE IF EXISTS films_import_queue;

CREATE TABLE films_import_queue
(
    id        INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    file      VARCHAR(255),
    worker_id INT          DEFAULT NULL,
    status    INT NOT NULL DEFAULT 0,
    error     TEXT
);

DROP TABLE IF EXISTS users;

CREATE TABLE users
(
    id      INT         NOT NULL AUTO_INCREMENT PRIMARY KEY,
    login   VARCHAR(32) NOT NULL UNIQUE,
    ps_hash VARCHAR(32) NOT NULL,
    token   VARCHAR(32) DEFAULT NULL
);

INSERT INTO users (login, ps_hash, token)
VALUES ('admin', '4f37f6439fcc472d9f283eabdbbc9059', '8315083c8fa322d6032438cdb722a578');
