<?php

namespace Core\utils;

/**
 * Class HtmlHelper
 * @package Core\utils
 */
class HtmlHelper
{
    /**
     * @param string $name
     * @param array $options
     * @param string|null $content
     * @param bool $withCloseTag
     * @return string
     */
    public static function tag(
        string $name, array $options = [], ?string $content = null, bool $withCloseTag = true
    ): string
    {
        $attributes = [];
        if (!empty($options['attributes'])) {
            foreach ($options['attributes'] as $field => $value) {
                $attributes[] = sprintf('%s="%s"', $field, $value);
            }
        }
        $attributes = implode(' ', $attributes);
        if ($withCloseTag) {
            return sprintf('<%s %s>%s</%s>', $name, $attributes, $content, $name);
        }
        return sprintf('<%s %s/>', $name, $attributes);
    }

    /**
     * @param string $src
     * @param array $options
     * @return string
     */
    public static function img(string $src, array $options = []): string
    {
        $options['attributes']['src'] = $src;
        return self::tag('img', $options, null, false);
    }

    /**
     * @param string $href
     * @param $content
     * @param array $options
     * @return string
     */
    public static function a(string $href, $content, array $options = []): string
    {
        $options['attributes']['href'] = $href;
        return self::tag('a', $options, $content);
    }
}
