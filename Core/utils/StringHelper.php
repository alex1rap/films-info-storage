<?php

namespace Core\utils;

/**
 * Class StringHelper
 * @package Core\utils
 */
class StringHelper
{
    /**
     * @param string $input
     * @return string
     */
    public static function getSnakeCaseFromCamelCase(string $input): string
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }

    /**
     * @param $string
     * @param false $capitalizeFirstCharacter
     * @return string|string[]
     */
    public static function getCamelCase($string, $capitalizeFirstCharacter = false)
    {

        $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $string)));

        if (!$capitalizeFirstCharacter) {
            $str[0] = strtolower($str[0]);
        }

        return $str;
    }

    /**
     * @param string $input
     * @return string
     */
    public static function getShortClassName(string $input): string
    {
        $path = explode('\\', $input);
        return array_pop($path);
    }


    /**
     * @param string $input
     * @param bool $capitalizeFirstCharacter
     * @return string
     */
    public function getCamelCaseFromSnakeCase(string $input, bool $capitalizeFirstCharacter = false): string
    {
        $result = str_replace(' ', '', ucwords(str_replace('_', ' ', $input)));
        if (!$capitalizeFirstCharacter) {
            $result[0] = strtolower($result[0]);
        }
        return $result;
    }
}
