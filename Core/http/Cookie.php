<?php

namespace Core\http;

use Exception;

/**
 * Class Cookie
 * @package Core\http
 */
class Cookie
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string|null
     */
    protected $value;

    /**
     * @var array
     */
    protected $params = ['path' => '/'];

    /**
     * Cookie constructor.
     * @param string $name
     * @param string|null $value
     * @param array $params
     * @throws Exception
     */
    public function __construct(string $name, ?string $value, array $params = [])
    {
        if (empty($name)) {
            throw new Exception('Cookie name cannot be empty.');
        }
        $this->name = $name;
        $this->value = $value;
        $this->params = array_replace_recursive($this->params, $params);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string|null $value
     */
    public function setValue(?string $value): void
    {
        $this->value = $value;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @param $name
     * @param $value
     */
    public function setParam($name, $value): void
    {
        $this->params[$name] = $value;
    }

    /**
     * @param array $params
     */
    public function setParams(array $params): void
    {
        $this->params = $params;
    }
}
