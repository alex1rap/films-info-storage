<?php

namespace Core\http\methods;

/**
 * Class Method
 * @package Core\http\methods
 */
abstract class Method
{
    /**
     * @var array
     */
    private $data = [];

    /**
     *
     */
    public function init(): void {}

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return $this->data;
    }

    /**
     * @param $key
     * @param null $defaultValue
     * @return string
     */
    public function get($key, $defaultValue = null): ?string
    {
        return empty($this->data[$key]) ? $defaultValue : $this->data[$key];
    }

    /**
     * @param $key
     * @param $value
     */
    public function set($key, $value): void
    {
        $this->data[$key] = $value;
    }
}
