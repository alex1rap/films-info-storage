<?php

namespace Core\http\methods;

class Post extends Method {

    public function init(): void
    {
        $this->setData($_POST);
    }
}
