<?php

namespace Core\http\methods;

class Get extends Method {
    public function init(): void
    {
        $this->setData($_GET);
    }
}
