<?php

namespace Core\http;

use Core\base\File;
use Core\http\methods\Get;
use Core\http\methods\Method;
use Core\http\methods\Post;
use Exception;

/**
 * Class Request
 * @package Core\web
 */
class Request extends Method
{
    /**
     * @var Get
     */
    public $get;

    /**
     * @var Post
     */
    public $post;

    /**
     * @var Cookie[]
     */
    public $cookies = [];

    /**
     * @var File[]
     */
    public $files = [];

    /**
     * @var string
     */
    protected $controller;

    /**
     * @var string
     */
    protected $action;

    /**
     * @var string|null
     */
    protected $route;

    /**
     * @var array
     */
    protected $params = [];

    /**
     *
     */
    public function init(): void
    {
        $this->get = new Get();
        $this->get->init();
        $this->post = new Post();
        $this->post->init();
        $this->files = $this->_getFiles();
        $this->cookies = $this->_getCookies();
        $this->setData($_REQUEST);
    }

    /**
     * @return array
     */
    private function _getFiles(): array
    {
        return array_map(function ($fileData) {
            return new File($fileData);
        }, $_FILES);
    }

    /**
     * @return array
     * @throws Exception
     */
    private function _getCookies(): array
    {
        $cookies = [];
        foreach ($_COOKIE as $key => $value) {
            $cookies[$key] = new Cookie($key, $value);
        }
        return $cookies;
    }

    /**
     * @return string
     */
    public function getController(): string
    {
        return $this->controller;
    }

    /**
     * @param string $controller
     */
    public function setController(string $controller): void
    {
        $this->controller = $controller;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    public function setAction(string $action): void
    {
        $this->action = $action;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @param array $params
     */
    public function setParams($params = []): void
    {
        $this->params = $params;
        foreach ($params as $param => $value) {
            if (!is_int($param)) {
                $this->get->set($param, $value);
                $this->set($param, $value);
            }
        }
    }

    /**
     * @param string $key
     * @return Cookie|null
     */
    public function getCookie(string $key): ?string
    {
        return empty($this->cookies[$key]) ? null : $this->cookies[$key]->getValue();
    }

    /**
     * @param string $key
     * @param string $value
     * @param array $params
     * @throws Exception
     */
    public function setCookie(string $key, string $value, array $params = []): void
    {
        $this->cookies[$key] = new Cookie($key, $value, $params);
    }

    /**
     * @param string $key
     * @throws Exception
     */
    public function removeCookie(string $key): void
    {
        $this->cookies[$key] = new Cookie($key, null);
    }

    /**
     * @return string|null
     */
    public function getRoute(): ?string
    {
        return $this->route;
    }

    /**
     * @param string|null $route
     */
    public function setRoute(?string $route): void
    {
        $this->route = $route;
    }
}
