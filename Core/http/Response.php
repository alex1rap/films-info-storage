<?php

namespace Core\http;

use App;
use Core\http\Cookie;

/**
 * Class Response
 * @package Core\web
 */
class Response
{
    /**
     * @var array
     */
    protected $header = [];

    /**
     * @var array
     */
    protected $redirect = [];

    /**
     * @var Cookie[]
     */
    protected $cookies = [];

    /**
     * @var string
     */
    protected $content = "";

    public function __construct()
    {
        foreach (App::$app->request->cookies as $cookie) {
            $this->setCookie($cookie);
        }
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->header;
    }

    /**
     * @param $key
     * @param $value
     */
    public function setHeader($key, $value)
    {
        $this->header[$key] = $value;
    }

    /**
     * @return Cookie[]
     */
    public function getCookies(): array
    {
        return $this->cookies;
    }

    /**
     * @param Cookie $cookie
     */
    public function setCookie(Cookie $cookie): void
    {
        $this->cookies[$cookie->getName()] = $cookie;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content)
    {
        $this->content = $content;
    }

    /**
     * @return array
     */
    public function getRedirect(): array
    {
        return $this->redirect;
    }

    /**
     * @param string $url
     * @param array $options
     */
    public function redirect(string $url, array $options = [])
    {
        $httpCode = empty($options['code']) ? 301 : $options['code'];
        $this->redirect = [
            'url' => $url,
            'code' => $httpCode
        ];
    }
}
