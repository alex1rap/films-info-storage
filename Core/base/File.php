<?php

namespace Core\base;

/**
 * Class File
 * @package Core\http
 */
class File
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $type;

    /**
     * @var int
     */
    private $size;

    /**
     * @var string
     */
    private $tmpName;

    /**
     * @var int
     */
    private $error;

    /**
     * File constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->name = $data['name'];
        $this->type = $data['type'];
        $this->size = $data['size'];
        $this->tmpName = $data['tmp_name'];
        $this->error = $data['error'];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @return string
     */
    public function getTmpName(): string
    {
        return $this->tmpName;
    }

    /**
     * @return int
     */
    public function getError(): int
    {
        return $this->error;
    }

    /**
     * @param string $path
     * @return bool
     */
    public function saveAs(string $path): bool
    {
        return move_uploaded_file($this->tmpName, $path);
    }
}
