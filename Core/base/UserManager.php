<?php

namespace Core\base;

use App;

class UserManager
{
    /**
     * @var User
     */
    private $_user;

    public function getUser(): User
    {
        if (null === $this->_user) {
            $token = empty($_SESSION['token']) ? null : $_SESSION['token'];
            if (null === $token) {
                return new User();
            }
            $user = new User();
            if ($user->loginByToken($token)) {
                $_SESSION['token'] = $user->getSessionToken();
            }
            $this->_user = $user;
        }
        return $this->_user;
    }
}
