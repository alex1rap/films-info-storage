<?php

namespace Core\base;

use App;

class User
{
    protected $id;
    protected $login;
    protected $passHash;
    protected $sessionToken;
    protected $isLoggedIn = false;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @return string
     */
    public function getSessionToken(): string
    {
        return sprintf('%s.%s', $this->id, $this->sessionToken);
    }

    /**
     * @return bool
     */
    public function isLoggedIn(): bool
    {
        return $this->isLoggedIn;
    }

    public function login(string $login, string $password): bool
    {
        $this->login = $login;
        $this->passHash = md5($password);
        return $this->_loadUser();
    }

    private function _loadUser(): bool
    {
        $sql = 'SELECT * FROM users WHERE login = ? AND ps_hash = ? LIMIT 1';
        $stmt = App::$app->dbConnection->getInstance()->prepare($sql);
        $stmt->execute([$this->login, $this->passHash]);
        $data = $stmt->fetchAll();
        if (empty($data)) {
            return false;
        }
        $userData = array_shift($data);
        $this->id = $userData['id'];
        $this->sessionToken = $this->_generateSessionToken($userData['token']);
        $_SESSION['token'] = $this->sessionToken;
        $this->isLoggedIn = true;
        return true;
    }

    public function loginByToken(string $tokenString): bool
    {
        [$id, $token] = explode('.', $tokenString);
        $sql = 'SELECT * FROM users WHERE id = ? AND token = ? LIMIT 1';
        $stmt = App::$app->dbConnection->getInstance()->prepare($sql);
        $stmt->execute([$id, $token]);
        $data = $stmt->fetchAll();
        if (empty($data)) {
            return false;
        }
        $this->isLoggedIn = true;
        $userData = array_shift($data);
        $this->sessionToken = $token;
        $this->id = $userData['id'];
        $this->login = $userData['login'];
        $this->passHash = $userData['ps_hash'];
        $_SESSION['token'] = $this->sessionToken;
        return true;
    }

    private function _generateSessionToken(string $token): string
    {
        return sprintf('%s.%s', $this->id, $token);
    }

    public function logout(): bool
    {
        unset($_SESSION['token']);
        return true;
    }
}
