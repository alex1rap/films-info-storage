<?php

namespace Core\database;

use Core\utils\StringHelper;
use Core\web\Model;
use PDO;

/**
 * Class Entity
 * @package Core\database
 */
abstract class Entity extends Model
{
    /**
     * @var PDO
     */
    private $_connection;

    /**
     * @var string[]
     */
    private $_deniedAttributes = ['_attributes', '_oldAttributes', '_deniedAttributes', '_connection'];

    protected $id = null;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getTableName(): string
    {
        return StringHelper::getSnakeCaseFromCamelCase(StringHelper::getShortClassName(self::class));
    }

    public function init(array $fields): void
    {
        parent::init($fields);
    }

    public function getDeniedFields(): array
    {
        return $this->_deniedAttributes;
    }
}
