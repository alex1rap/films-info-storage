<?php

namespace Core\database;

use Core\utils\StringHelper;
use PDO;
use ReflectionClass;
use ReflectionException;

/**
 * Class EntityManager
 * @package Core\database
 */
class EntityManager
{
    /**
     * @var PDO
     */
    private $_connection;

    /**
     * @var Entity[][]
     */
    private $_newEntities = [];

    /**
     * @var Entity[][]
     */
    private $_updatedEntities = [];

    /**
     * @var Entity[][]
     */
    private $_removedEntities = [];

    /**
     * @var Repository[]
     */
    private $_repositories = [];

    /**
     * EntityManager constructor.
     * @param $appInstance
     */
    public function __construct($appInstance)
    {
        /** @var PDO _connection */
        $this->_connection = $appInstance->dbConnection->getInstance();
    }

    /**
     * @return PDO
     */
    public function getConnection(): PDO
    {
        return $this->_connection;
    }

    /**
     * @param Entity $entity
     * @throws ReflectionException
     */
    public function persist(Entity $entity)
    {
        if (null === $entity->getId()) {
            $this->_newEntities[$entity->getTableName()][] = $entity;
        } else {
            $class = new ReflectionClass(get_class($entity));
            $fields = array_keys($class->getDefaultProperties());
            $data = [];
            foreach ($fields as $field) {
                $method = 'get' . ucfirst($field);
                $data[$field] = $entity->$method();
            }
            $entity->init($data);
            $this->_updatedEntities[$entity->getTableName()][$entity->getId()] = $entity;
        }
    }

    /**
     * @param Entity $entity
     */
    public function remove(Entity $entity)
    {
        $id = $entity->getId();
        $tableName = $entity->getTableName();
        if (!empty($this->_updatedEntities[$tableName][$id])) {
            unset($this->_updatedEntities[$tableName][$id]);
        }
        $this->_removedEntities[$tableName][$id] = $entity;
    }

    /**
     *
     */
    public function flush()
    {
        $this->insertEntities();
        $this->updateEntities();
        $this->removeEntities();
    }

    /**
     *
     */
    protected function insertEntities(): void
    {
        $queries = [];
        foreach ($this->_newEntities as $name => $entities) {
            if (empty($entities)) {
                continue;
            }
            $firstEntity = $entities[0];
            $tableName = $firstEntity->getTableName();
            $class = new ReflectionClass(get_class($firstEntity));
            $fields = array_keys($class->getDefaultProperties());
            $queries[] = $this->getInsertSqlQuery($tableName, $fields, $entities);
        }
        $this->executeQueries($queries);
        $this->_newEntities = [];
    }

    /**
     * @throws ReflectionException
     */
    protected function updateEntities()
    {
        $queries = [];
        foreach ($this->_updatedEntities as $name => $entities) {
            if (empty($entities)) {
                continue;
            }
            $firstEntity = $entities[array_key_first($entities)];
            $tableName = $firstEntity->getTableName();
            $class = new ReflectionClass(get_class($firstEntity));
            $fields = array_keys($class->getDefaultProperties());
            $tableQueries = [];
            $tableValues = [];
            foreach ($entities as $entity) {
                if (!$entity->isUpdated()) {
                    continue;
                }
                $query = $this->getUpdateSqlQuery($tableName, $fields, $entity, $entity->getId());
                $tableQueries[] = $query['sql'];
                $tableValues = array_merge($tableValues, $query['values']);
            }
            $queries[] = [
                'sql' => implode(';' . PHP_EOL, $tableQueries),
                'values' => $tableValues
            ];
        }
        $this->executeQueries($queries);
        $this->_newEntities = [];
    }

    /**
     *
     */
    protected function removeEntities()
    {
        $queries = [];
        foreach ($this->_removedEntities as $name => $entities) {
            $tableQueries = [];
            $tableValues = [];
            foreach ($entities as $id => $entity) {
                $tableQueries[] = sprintf('DELETE FROM %s WHERE id = ?', $name);
                $tableValues[] = $id;
            }
            $queries[] = [
                'sql' => implode(';' . PHP_EOL, $tableQueries),
                'values' => $tableValues
            ];
        }
        $this->executeQueries($queries);
    }

    /**
     * @param string $tableName
     * @param array $fields
     * @param Entity $entity
     * @param $id
     * @return array
     */
    protected function getUpdateSqlQuery(string $tableName, array $fields, Entity $entity, $id): array
    {
        $values = [];
        $sqlFields = implode(', ', array_map(function ($field) use ($entity, &$values) {
            $method = 'get' . ucfirst($field);
            $values[] = $entity->$method();
            return sprintf('%s = ?', StringHelper::getSnakeCaseFromCamelCase($field));
        }, $fields));
        $values[] = $id;
        return [
            'sql' => sprintf('UPDATE %s SET %s WHERE id = ?', $tableName, $sqlFields),
            'values' => $values
        ];
    }

    /**
     * @param string $tableName
     * @param array $fields
     * @param array $entities
     * @return array
     */
    protected function getInsertSqlQuery(string $tableName, array $fields, array $entities): array
    {
        $sqlFields = implode(', ', array_map(function ($property) {
            return StringHelper::getSnakeCaseFromCamelCase($property);
        }, $fields));
        $mask = [];
        $values = [];
        foreach ($entities as $entity) {
            $maskRow = [];
            foreach ($fields as $field) {
                $method = 'get' . ucfirst($field);
                $maskRow[] = '?';
                $values[] = $entity->$method();
            }
            $mask[] = sprintf('(%s)', implode(',', $maskRow));
        }
        return [
            'sql' => sprintf('INSERT INTO %s (%s) VALUES %s',
                $tableName,
                $sqlFields,
                implode(',', $mask)),
            'values' => $values
        ];
    }

    /**
     * @param array $queries
     * @return bool
     */
    protected function executeQueries(array $queries): bool
    {
        $this->_connection->beginTransaction();
        foreach ($queries as $query) {
            $stmt = $this->_connection->prepare($query['sql']);
            $stmt->execute($query['values']);
        }
        return $this->_connection->commit();
    }

    /**
     * @param string $className
     * @return Repository|null
     */
    public function getRepository(string $className): ?Repository
    {
        if (empty($this->_repositories[$className])) {
            $repositoryName = 'App\\repositories\\' . StringHelper::getShortClassName($className) . 'Repository';
            $this->_repositories[$className] = new $repositoryName($className);
        }
        return $this->_repositories[$className] ?? null;
    }
}
