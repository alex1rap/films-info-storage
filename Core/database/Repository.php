<?php

namespace Core\database;

use App;
use PDO;

/**
 * Class Repository
 * @package Core\database
 */
abstract class Repository
{
    /**
     * @var PDO
     */
    private $_connection;

    /**
     *
     */
    protected const DEFAULT_ORDER_BY = 'id DESC';
    /**
     *
     */
    protected const DEFAULT_WHERE = '1';

    /**
     * @var string
     */
    protected $entityClass;

    /**
     * Repository constructor.
     * @param string $entityClass
     */
    public function __construct(string $entityClass)
    {
        $this->entityClass = $entityClass;
        $this->_connection = App::$app->dbConnection->getInstance();
    }

    /**
     * @return string
     */
    abstract public function getTableName(): string;

    /**
     * @param $id
     * @return Entity|null
     */
    public function findOne($id): ?Entity
    {
        $sql = sprintf('SELECT * FROM %s WHERE id = ? LIMIT 1', $this->getTableName());
        $stmt = $this->_connection->prepare($sql);
        $stmt->execute([$id]);
        $result = $stmt->fetchAll();
        if (empty($result)) {
            return null;
        }
        $className = $this->entityClass;
        /** @var Entity $entity */
        $entity = new $className();
        $entity->init(array_shift($result));
        return $entity;
    }

    /**
     * @param array $where
     * @param string|null $orderBy
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function findBy(array $where, ?string $orderBy = null, int $offset = 0, int $limit = 0): array
    {
        if (empty($where['sql'])) {
            $where['sql'] = self::DEFAULT_WHERE;
        }
        if (empty($where['params'])) {
            $where['params'] = [];
        }
        if (empty($orderBy)) {
            $orderBy = self::DEFAULT_ORDER_BY;
        }
        $limitSql = $limit > 0 ? sprintf('LIMIT %d, %d', $offset, $limit) : '';
        $sql = sprintf('SELECT * FROM %s WHERE %s ORDER BY %s %s',
            $this->getTableName(),
            $where['sql'],
            $orderBy,
            $limitSql);

        $stmt = $this->_connection->prepare($sql);
        $stmt->execute($where['params']);
        return $stmt->fetchAll();
    }

    public function getCount(array $where = []): int
    {
        if (empty($where['sql'])) {
            $where['sql'] = self::DEFAULT_WHERE;
        }
        if (empty($where['params'])) {
            $where['params'] = [];
        }
        $sql = sprintf('SELECT COUNT(*) as rows_count FROM %s WHERE %s',
            $this->getTableName(),
            $where['sql']);

        $stmt = $this->_connection->prepare($sql);
        $stmt->execute($where['params']);
        return $stmt->fetchColumn();
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        $sql = sprintf('SELECT * FROM %s', $this->getTableName());
        $stmt = $this->_connection->prepare($sql);
        return $stmt->execute() ? $stmt->fetchAll() : [];
    }

    /**
     * @param array $data
     */
    public function insert(array $data)
    {
        $fields = implode(',', array_keys($data));
        $mask = [];
        $values = $this->getMaskAndValues($data, $mask);
        $sql = sprintf("INSERT INTO %s (%s) VALUES %s",
            $this->getTableName(),
            $fields,
            $mask);
        $stmt = $this->_connection->prepare($sql);
        $stmt->execute($values);
    }

    /**
     * @param array $data
     * @param array $mask
     * @return array
     */
    protected function getMaskAndValues(array $data, array &$mask = []): array
    {
        $values = [];
        foreach ($data as $field => $value) {
            if (is_array($value)) {
                $values = array_merge($values, $this->getMaskAndValues($value));
            } else {
                $mask[] = '?';
                $values = array_merge($values, [$value]);
            }
        }
        return $values;
    }

    /**
     * @return PDO
     */
    public function getConnection(): PDO
    {
        return $this->_connection;
    }
}
