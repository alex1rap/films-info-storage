<?php

namespace Core\database;

use App;
use PDO;

class Connection
{
    /**
     * @var string[]|array
     */
    private $config = [
        'driver' => 'mysql',
        'host' => 'localhost',
        'user' => 'root',
        'pass' => '',
        'name' => 'dbname'
    ];

    /**
     * @var PDO
     */
    private $instance = null;

    /**
     * Connection constructor.
     * @param $appInstance
     */
    public function __construct($appInstance)
    {
        if (!empty($appInstance->config['db'])) {
            $this->config = array_replace_recursive($this->config, $appInstance->config['db']);
        }
        $appInstance->dbConnection = $this;
    }

    /**
     * @return PDO
     */
    public function getInstance(): PDO
    {
        if (null === $this->instance) {
            $dsn = sprintf(
                '%s:dbname=%s;host=%s',
                $this->config['driver'],
                $this->config['name'],
                $this->config['host']
            );
            $this->instance = new PDO(
                $dsn,
                $this->config['user'],
                $this->config['pass'],
            );
        }
        return $this->instance;
    }
}
