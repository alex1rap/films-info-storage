<?php

namespace Core\web;

use Core\utils\StringHelper;
use ReflectionClass;

/**
 * Class Model
 * @package Core\web
 */
abstract class Model
{
    private $_attributes = [];
    private $_oldAttributes = [];

    /**
     * @param array $fields
     */
    public function init(array $fields): void
    {
        foreach ($fields as $field => $value) {
            $field = StringHelper::getCamelCase($field);
            if (property_exists($this, $field)) {
                $method = 'set' . ucfirst($field);
                $this->$method($value);
                $this->_attributes[$field] = $value;
            }
        }
    }

    /**
     * @return array
     */
    protected function getDeniedFields(): array
    {
        return [];
    }

    public function isUpdated(): bool
    {
        return $this->_attributes != $this->_oldAttributes;
    }

    public function getAttributes(): array
    {
        return $this->_attributes;
    }

}
