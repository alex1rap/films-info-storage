<?php

namespace Core\web;

use App;
use Core\database\EntityManager;
use Core\http\Response;
use Exception;

/**
 * Class Controller
 * @package Core\web
 */
class Controller
{
    /**
     * @var View
     */
    private $_view;

    /**
     * @var EntityManager
     */
    private $_em;

    protected $request;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->_em = App::$app->entityManager;
        $this->request = App::$app->request;
        $this->_view = new View();
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager(): EntityManager
    {
        return $this->_em;
    }

    /**
     * @param string $viewName
     * @param array $params
     * @return string
     */
    public function render(string $viewName, array $params = []): string
    {
        return $this->_view->renderLayout($viewName, $params);
    }

    /**
     * @return Response|string|mixed
     */
    public function indexAction()
    {
        return 'Hello world!';
    }

    /**
     * @return string
     */
    public function error404Action(): string
    {
        http_response_code(404);
        return "<h1>Error 404!</h1><h2>Page was not found!</h2>";
    }

    /**
     * @return bool
     */
    protected function isAnonymous(): bool
    {
        return !App::$app->user->isLoggedIn();
    }

    /**
     * @param string $url
     * @param array $options
     * @return Response
     */
    protected function redirect(string $url, array $options = []): Response
    {
        $response = new Response();
        $response->redirect($url, $options);
        return $response;
    }

    /**
     * @param array $options
     * @return Response
     */
    protected function redirectToLogin(array $options = []): Response
    {
        return $this->redirect('/auth/signIn', $options);
    }

    /**
     * @param array $options
     * @return Response
     */
    protected function redirectToHome(array $options = []): Response
    {
        return $this->redirect('/', $options);
    }

    /**
     * @param string $key
     * @return string|null
     */
    protected function getFlash(string $key): ?string
    {
        return $this->request->getCookie($key);
    }

    /**
     * @param string $key
     * @param string $value
     * @throws Exception
     */
    protected function setFlash(string $key, string $value): void
    {
        $this->request->setCookie($key, $value);
    }

    /**
     * @param string $key
     * @throws Exception
     */
    protected function removeFlash(string $key): void
    {
        if (!empty($this->getFlash($key))) {
            $this->request->removeCookie($key);
        }
    }
}
