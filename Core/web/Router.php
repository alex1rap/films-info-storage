<?php

namespace Core\web;

use App;
use Core\http\Request;
use Core\utils\StringHelper;
use Exception;

class Router
{
    protected $config = [
        "errors" => [
            "error404" => [
                "controller" => "Core\\web\\Controller",
                "action" => "error404Action"
            ]
        ]
    ];

    public function __construct()
    {
        if (!empty(App::$app->config['Router'])) {
            $this->config = array_replace_recursive($this->config, App::$app->config['Router']);
        }
    }

    public function parseUrl(string $url): Request
    {
        $params = [];
        $route = null;
        foreach ($this->config['rules'] as $rule => $route) {
            if (preg_match(sprintf('~^%s/?(\?(?<params>.*))?$~uisU', $rule), $url, $params)) {
                $url = '/' . $route . (empty($params['params']) ? '' : '?' . $params['params']);
                break;
            }
        }
        if (preg_match('~^/(?<path>.*/)?(?<controller>[^/]+)/(?<action>[^/]+)/?(\?(?<params>.*))?$~uisU', $url, $matches)) {
            if (!empty($matches['controller']) && !empty($matches['action'])) {
                $route = $route ?? $matches['path'] . $matches['controller'] . '/' . $matches['action'];
                $controller = str_replace('/', '\\', sprintf(
                    'App\\controllers\\%s%sController',
                    $matches['path'],
                    ucfirst($matches['controller'])
                ));
                $action = sprintf('%sAction', $matches['action']);
                if (!empty($matches['params'])) {
                    foreach (explode('&', $matches['params']) as $item) {
                        $item = explode('=', $item, 2);
                        $params[$item[0]] = empty($item[1]) ? true : $item[1];
                    }
                }
            }
        }
        if (empty($controller) || empty($action)) {
            if (empty($this->config['errors']) ||
                empty($this->config['errors']['error404']) ||
                empty($this->config['errors']['error404']['controller']) ||
                empty($this->config['errors']['error404']['action'])) {
                throw new Exception("No route found for URI '{$url}'");
            }
            $errorsConfig = $this->config['errors']['error404'];
            $controller = $errorsConfig['controller'];
            $action = $errorsConfig['action'];
        }

        $request = new Request();
        $request->init();
        $request->setController($controller);
        $request->setAction($action);
        $request->setRoute($route);
        $request->setParams($params);
        return $request;
    }
}
