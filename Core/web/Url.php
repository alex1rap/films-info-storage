<?php

namespace Core\web;

use App;
use Core\utils\StringHelper;

/**
 * Class Url
 * @package Core\web
 */
class Url
{
    /**
     * @var array
     */
    private static $_rules = [];

    /**
     * @param string $route
     * @param array $params
     * @return string
     */
    public static function to(string $route, array $params = []): string
    {
        $route = trim($route);
        if (preg_match('~^((https?:)?//|/)~', $route)) {
            return self::makeUrl($route, $params);
        }
        return self::byRules($route, $params);
    }

    /**
     * @param string $url
     * @param array $params
     * @return string
     */
    protected static function makeUrl(string $url, array $params = []): string
    {
        return empty($params) ? $url : implode('?', [$url, http_build_query($params)]);
    }

    /**
     * @param string $route
     * @param array $params
     * @return string
     */
    protected static function byRules(string $route, array $params = []): string
    {
        $key = self::getRuleKey($route, $params);
        if (empty(self::$_rules[$key])) {
            self::generateRule($route, $params);
        }
        return empty(self::$_rules[$key]) ?
            self::makeUrl($route, $params) :
            self::byRule(self::$_rules[$key], $params);
    }

    /**
     * @param string $rule
     * @param array $params
     * @return string
     */
    protected static function byRule(string $rule, array $params = []): string
    {
        $queryString = [];
        foreach ($params as $field => $value) {
            $pattern = sprintf("~\{%s\}~", $field);
            if (preg_match($pattern, $rule, $matches)) {
                if (preg_match($matches[0], $field)) {
                    $rule = preg_replace($pattern, $value, $rule);
                    continue;
                }
            }
            $queryString[$field] = $value;
        }
        return self::makeUrl($rule, $queryString);
    }

    /**
     * @param string $route
     * @param array $params
     * @return string
     */
    protected static function getRuleKey(string $route, array $params = []): string
    {
        ksort($params);
        return implode('_', array_merge([$route], array_keys($params)));
    }

    /**
     * @param string $route
     * @param array $params
     */
    protected static function generateRule(string $route, array $params): void
    {
        $customRules = empty(App::$app->config['Router']['rules']) ? [] : App::$app->config['Router']['rules'];
        foreach ($customRules as $key => $customRule) {
            if ($route == $customRule) {
                $rule = $key;
                foreach ($params as $field => $value) {
                    $pattern = sprintf('~\(\?\<(%s)\>[^)]+\)~', $field);
                    $rule = preg_replace($pattern, sprintf('{%s}', $field), $rule);
                }
                self::$_rules[self::getRuleKey($route, $params)] = $rule;
            }
        }
    }
}
