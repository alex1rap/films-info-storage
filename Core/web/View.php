<?php

namespace Core\web;

use App;

/**
 * Class View
 * @package Core\web
 */
class View
{
    /**
     * @var array
     */
    protected $config = [
        'viewPath' => FS_ROOT . '/App/views',
        "viewExt" => 'php',
        'mainLayout' => 'main'
    ];

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $headEnd = "";

    /**
     * @var string
     */
    protected $pageEnd = "";

    /**
     * @var string
     */
    protected $content = "";

    /**
     * View constructor.
     */
    public function __construct()
    {
        $this->title = App::$app->config['name'];
        if (!empty(App::$app->config['View'])) {
            $this->config = array_replace_recursive($this->config, App::$app->config['View']);
        }
    }

    /**
     * @param string $viewName
     * @param array $params
     * @return false|string
     */
    public function render(string $viewName, array $params = [])
    {
        ob_start();
        extract($params);
        include sprintf('%s/%s.%s', $this->config['viewPath'], $viewName, $this->config['viewExt']) . '';
        return ob_get_clean();
    }

    /**
     * @param string $viewName
     * @param array $params
     * @return string
     */
    public function renderLayout(string $viewName, array $params = []): string
    {
        $this->content = $this->render($viewName, $params);
        return $this->render($this->config['mainLayout']);
    }

    protected function print(?string $value): string
    {
        return htmlspecialchars($value ?? '');
    }
}
