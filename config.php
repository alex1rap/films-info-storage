<?php

$dbConfigFilename = __DIR__ . DIRECTORY_SEPARATOR . 'db_config.php';
$dbConfig = file_exists($dbConfigFilename) ? require_once $dbConfigFilename . '' : [];

return [
    "name" => "Films",
    "db" => $dbConfig,
    "View" => [
        "viewExt" => "phtml"
    ],
    "Router" => [
        "rules" => [
            "/" => "films/index",
            "/film/(?<id>[0-9]+)" => "films/show",
            "/queues" => "filmsImportingHistory/index"
        ],
    ],
    "Pagination" => [
        "itemsPerPage" => 15
    ]
];
