# Документация

## [Корень](../README.md) > Файлы конфигурации

### Назначение и структура конфигурационных файлов:

### `config.php`

Основной конфигурационный файл для работы проекта (переопределяет стандартную конфигурацию для указанных классов):
```php
<?php

$dbConfigFilename = __DIR__ . DIRECTORY_SEPARATOR . 'db_config.php';
$dbConfig = file_exists($dbConfigFilename) ? require_once $dbConfigFilename . '' : [];

return [
    "name" => "Films", /* Название проекта, будет выводиться наверху страницы как бренд */
    "db" => $dbConfig, /* Переопределение настроек подключения к БД */
    "View" => [ /* переопределение конфигурации для основного класса вида */
        "viewExt" => "phtml" /* формат файлов вида */
    ],
    "Router" => [/* Переопределение настроек маршрутизации */
        "rules" => [/* Назначение пользовательских контроллеров и методов для определенных маршрутов */
            "/" => "App\\controllers\\FilmsController::indexAction",
            "/film/(?<id>[0-9]+)" => "App\\controllers\\FilmsController::showAction", /* значения параметров вида */
            /* `?<ключ>RegExp` будут доступны в объекте запроса (`$request->get->get('ключ')`) */
        ],
        "errors" => [/* переопределение обработчика ошибок (в частности, 404) */
            "error404" => [
                "controller" => "Core\\web\\Controller",
                "action" => "error404Action"
            ]
        ]
    ]
];

```

### `db_config.php`

Файл настроек подключения к БД:
```php
<?php

return [
    'driver' => 'mysql', /* драйвер СУБД, сейчас поддерживается только `mysql` */
    'host' => 'localhost', /* IP-адрес или домен, по которому можно достучаться до СУБД */
    'user' => 'root', /* имя пользователя, у которого есть полномочия к нужной БД */
    'pass' => '', /* пароль данного пользователя СУБД */
    'name' => 'db_name' /* имя нужной базы данных */
];

```

### `local_webserver_router.php`

Файл маршрутизации для локального веб-сервера PHP:
```php
<?php

if (preg_match(sprintf('~\\.(?:%s)$~uisU', implode('|', [
    'png', 'jpg', 'gif', 'ico', 'js', 'css', 'ttf', 'txt', 'woff', 'woff2', 'map' 
    /* форматы файлов, которые нужно отдавать браузеру "как есть", без обработки интерпретатором PHP */
])), parse_url($_SERVER['REQUEST_URI'])['path'])) {
    return false;
}

require_once __DIR__ . DIRECTORY_SEPARATOR . 'index.php';

```

### [В корень](../README.md)
