<?php

if (preg_match(sprintf('~\\.(?:%s)$~uisU', implode('|', [
    'png', 'jpg', 'gif', 'ico', 'js', 'css', 'ttf', 'txt', 'woff', 'woff2', 'map'
])), parse_url($_SERVER['REQUEST_URI'])['path'])) {
    return false;
}

require_once __DIR__ . DIRECTORY_SEPARATOR . 'index.php';
