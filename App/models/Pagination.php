<?php

namespace App\models;

use App;
use Core\web\Model;
use Core\web\Url;

/**
 * Class Pagination
 * @package App\models
 */
class Pagination extends Model
{
    /**
     * @var array
     */
    protected $config = [
        'itemsPerPage' => 10,
    ];

    /**
     * @var string|null
     */
    protected $route;

    /**
     * @var int
     */
    protected $page;

    /**
     * @var int
     */
    protected $offset = 0;

    /**
     * @var int
     */
    protected $itemsCount;

    /**
     * Pagination constructor.
     * @param int $itemsCount
     * @param array $config
     */
    public function __construct(int $itemsCount, array $config = [])
    {
        $defaultConfig = empty(App::$app->config['Pagination']) ? [] : App::$app->config['Pagination'];
        $this->config = array_replace_recursive($this->config, $defaultConfig, $config);
        $page = App::$app->request->get->get('page', 1);
        $this->page = $page > 0 ? $page : 1;
        $this->offset = $this->getLimit() * ($this->page - 1);
        $this->route = App::$app->request->getRoute();
        $this->itemsCount = $itemsCount;
    }

    /**
     * @return array
     */
    public function getPages(): array
    {
        $pagesCount = ceil($this->itemsCount / $this->config['itemsPerPage']);
        return $pagesCount > 7 ? $this->generateManyPages($pagesCount) : $this->generateFewPages(1, $pagesCount);
    }

    /**
     * @param int $count
     * @return array
     */
    protected function generateManyPages(int $count): array
    {
        $params = App::$app->request->get->all();
        $pages = [];
        if ($this->page < 5) {
            $pages = $this->generateFewPages(1, 5);
            $pages[] = $this->generateEmptyPage();
            $pages[] = $this->generatePage($count, $params);
        } elseif ($this->page >= $count - 3) {
            $pages[] = $this->generatePage(1, $params);
            $pages[] = $this->generateEmptyPage();
            $pages = array_merge($pages, $this->generateFewPages($count - 4, $count));
        } else {
            $pages[] = $this->generatePage(1, $params);
            $pages[] = $this->generateEmptyPage();
            $pages = array_merge($pages, $this->generateFewPages($this->page - 1, $this->page + 1));
            $pages[] = $this->generateEmptyPage();
            $pages[] = $this->generatePage($count, $params);
        }
        return $pages;
    }

    /**
     * @param int $page
     * @param array $params
     * @return array
     */
    protected function generatePage(int $page, array $params): array
    {
        $params['page'] = $page;
        $link = Url::to($this->route, $params);
        return [
            'link' => $link,
            'isActive' => $page == $this->page,
            'isDisabled' => $page == $this->page,
            'title' => $page
        ];
    }

    protected function generateEmptyPage(): array
    {
        return [
            'link' => '',
            'isActive' => false,
            'isDisabled' => true,
            'title' => '...'
        ];
    }

    /**
     * @param int $start
     * @param int $count
     * @return array
     */
    protected function generateFewPages(int $start, int $count): array
    {
        $params = App::$app->request->get->all();
        $pages = [];
        for ($i = $start; $i <= $count; $i++) {
            $pages[] = $this->generatePage($i, $params);
        }
        return $pages;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->config['itemsPerPage'];
    }
}
