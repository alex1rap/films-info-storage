<?php

namespace App\controllers;

use App\entities\FilmsImportQueue;
use App\models\Pagination;
use App\repositories\FilmsImportQueueRepository;
use Core\web\Controller;

class FilmsImportingHistoryController extends Controller
{

    public function indexAction()
    {
        if ($this->isAnonymous()) {
            return $this->redirectToLogin();
        }
        /** @var FilmsImportQueueRepository $repository */
        $repository = $this->getEntityManager()->getRepository(FilmsImportQueue::class);
        $count = $repository->getCount();
        $pagination = new Pagination($count);
        $history = $repository->findBy([], '', $pagination->getOffset(), $pagination->getLimit());
        return $this->render('films_importing_history/index', [
            'history' => $history,
            'pages' => $pagination->getPages(),
            'statuses' => FilmsImportQueue::getAvailableStatuses(),
            'storage' => FilmsImportQueue::getFilesStorageUrl()
        ]);
    }

}
