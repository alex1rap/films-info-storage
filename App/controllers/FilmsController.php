<?php

namespace App\controllers;

use App\entities\FilmsImportQueue;
use App\entities\Film;
use App\models\Pagination;
use App\repositories\FilmRepository;
use Core\http\Response;
use Core\utils\HtmlHelper;
use Core\web\Controller;
use Core\web\Url;
use Exception;
use ReflectionException;

/**
 * Class FilmsController
 * @package App\controllers
 */
class FilmsController extends Controller
{
    /**
     * @return Response|string
     * @throws Exception
     */
    public function indexAction()
    {
        $search = $this->request->get->get('search');
        $message = $this->getFlash('filmsMessage') ?? '';
        $this->removeFlash('filmsMessage');
        /** @var FilmRepository $repository */
        $repository = $this->getEntityManager()->getRepository(Film::class);
        $count = $repository->getCountByTitleAndStars($search);
        $pagination = new Pagination($count);
        $films = $repository->findByTitleAndStars($search, $pagination->getOffset(), $pagination->getLimit());
        return $this->render('films/index', [
            'films' => $films,
            'search' => $search,
            'message' => $message,
            'availableFormats' => Film::getAvailableFormats(),
            'pages' => $pagination->getPages()
        ]);
    }

    /**
     * @return Response|string
     */
    public function showAction()
    {
        $id = (int) $this->request->get('id');
        /** @var FilmRepository $repository */
        $repository = $this->getEntityManager()->getRepository(Film::class);
        /** @var Film $film */
        $film = $repository->findOne($id);
        $removeHash = $this->_getFilmHash($film);
        return $this->render('films/show', [
            'film' => $film,
            'removeHash' => $removeHash
        ]);
    }

    /**
     * @return Response
     * @throws ReflectionException
     * @throws Exception
     */
    public function addAction(): Response
    {
        if ($this->isAnonymous()) {
            return $this->redirectToLogin();
        }
        $title = $this->request->post->get('title');
        $year = (int) $this->request->post->get('year');
        $format = $this->request->post->get('format');
        $stars = $this->request->post->get('stars');
        $film = new Film();
        $film->init([
            'title' => $title,
            'releaseYear' => $year,
            'format' => $format,
            'stars' => $stars,
        ]);
        $em = $this->getEntityManager();
        $em->persist($film);
        $em->flush();
        $this->setFlash('filmsMessage', 'Film was added to list.');
        return $this->redirectToHome();
    }

    /**
     * @return Response
     * @throws Exception
     */
    public function removeAction(): Response
    {
        if ($this->isAnonymous()) {
            return $this->redirectToLogin();
        }
        $id = (int) $this->request->post->get('id');
        $hash = $this->request->post->get('hash');
        /** @var FilmRepository $repository */
        $repository = $this->getEntityManager()->getRepository(Film::class);
        /** @var Film $film */
        $film = $repository->findOne($id);
        $removeHash = $this->_getFilmHash($film);
        if ($hash == $removeHash) {
            $em = $this->getEntityManager();
            $em->remove($film);
            $em->flush();
        }
        $this->setFlash('filmsMessage', 'Film was removed from list.');
        return $this->redirectToHome();
    }

    /**
     * @return Response
     * @throws ReflectionException
     * @throws Exception
     */
    public function importAction(): Response
    {
        if ($this->isAnonymous()) {
            return $this->redirectToLogin();
        }
        $em = $this->getEntityManager();
        $file = empty($this->request->files['films']) ? null : $this->request->files['films'];
        $fileName = sprintf('%f.txt', microtime(true));
        if ($file->saveAs(FilmsImportQueue::FILE_PATH . '/' . $fileName)) {
            $queue = new FilmsImportQueue();
            $queue->setFile($fileName);
            $em->persist($queue);
            $em->flush();
        }
        $this->setFlash('filmsMessage', sprintf('Films importing was added to queue. You can see %s.',
            HtmlHelper::a(Url::to('filmsImportingHistory/index'), 'history')));
        return $this->redirectToHome();
    }

    /**
     * @param Film $film
     * @return string
     */
    private function _getFilmHash(Film $film): string
    {
        return md5(sprintf('%d_%s', $film->getId(), $film->getTitle()));
    }
}
