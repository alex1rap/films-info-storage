<?php

namespace App\controllers;

use App;
use Core\http\Response;
use Core\web\Controller;
use Exception;

class AuthController extends Controller
{
    /**
     * @return Response|string
     * @throws Exception
     */
    public function signInAction()
    {
        $user = App::$app->user;
        if ($user->isLoggedIn()) {
            return $this->redirectToHome();
        }
        $login = $this->request->post->get('login', '');
        $pass = $this->request->post->get('pass', '');
        $errors = [];
        if (!empty($login) && $user->login($login, $pass)) {
            return $this->redirectToHome();
        } elseif (!empty($login)) {
            $errors[] = 'Invalid login or password.';
        }
        return $this->render('auth/sign_in', ['errors' => $errors]);
    }

    /**
     * @return Response
     */
    public function logoutAction(): Response
    {
        App::$app->user->logout();
        return $this->redirectToHome();
    }
}
