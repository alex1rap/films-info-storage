<?php

namespace App\entities;

use Core\database\Entity;

/**
 * Class FilesImportQueue
 * @package App\entities
 */
class FilmsImportQueue extends Entity
{
    /**
     *
     */
    public const FILE_PATH = FS_ROOT . '/storage/films';

    /**
     * @return string
     */
    public function getTableName(): string
    {
        return 'films_import_queue';
    }

    /**
     * @return string[]
     */
    public static function getAvailableStatuses(): array
    {
        return [
            '0' => 'Waiting',
            '1' => 'In Progress',
            '2' => 'Done',
            '-1' => 'Error'
        ];
    }

    /**
     * @return string
     */
    public static function getFilesStorageUrl(): string
    {
        return '/storage/films';
    }

    /**
     * @return string
     */
    public static function getFilesStorage(): string
    {
        return FS_ROOT . self::getFilesStorageUrl();
    }

    /**
     * @var string
     */
    private $file = null;

    /**
     * @var int|null
     */
    private $workerId = null;

    /**
     * @var int
     */
    private $status = 0;

    /**
     * @var
     */
    private $error = '';

    /**
     * @var int|null
     */
    protected $id = null;

    /**
     * @return mixed
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file): void
    {
        $this->file = $file;
    }

    /**
     * @return mixed
     */
    public function getWorkerId()
    {
        return $this->workerId;
    }

    /**
     * @param mixed $workerId
     */
    public function setWorkerId($workerId): void
    {
        $this->workerId = $workerId;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param mixed $error
     */
    public function setError($error): void
    {
        $this->error = $error;
    }
}
