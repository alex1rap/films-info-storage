<?php

namespace App\entities;

use Core\database\Entity;

/**
 * Class Film
 * @package App\entities
 */
class Film extends Entity
{
    public function getTableName(): string
    {
        return 'films';
    }

    /**
     * @return string[]
     */
    public static function getAvailableFormats(): array
    {
        return [
            'Blu-Ray', 'DVD', 'VHS'
        ];
    }

    /**
     * @var int|null
     */
    protected $id = null;

    /**
     * @var string
     */
    protected $title = '';

    /**
     * @var int
     */
    protected $releaseYear = '';

    /**
     * @var string
     */
    protected $format = '';

    /**
     * @var string
     */
    protected $stars = '';

    public function init(array $fields): void
    {
        parent::init($fields);
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getReleaseYear(): int
    {
        return $this->releaseYear;
    }

    /**
     * @param int $releaseYear
     */
    public function setReleaseYear(int $releaseYear): void
    {
        $this->releaseYear = $releaseYear;
    }

    /**
     * @return string
     */
    public function getFormat(): string
    {
        return $this->format;
    }

    /**
     * @param string $format
     */
    public function setFormat(string $format): void
    {
        $this->format = $format;
    }

    /**
     * @return string
     */
    public function getStars(): string
    {
        return $this->stars;
    }

    /**
     * @param string $stars
     */
    public function setStars(string $stars): void
    {
        $stars = implode(',', array_unique(array_map(function ($star) {
            return trim($star);
        }, explode(',', $stars))));
        $this->stars = $stars;
    }

}
