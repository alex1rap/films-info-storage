<?php

namespace App\repositories;

use App\entities\Film;
use Core\database\Repository;

/**
 * Class FilmRepository
 * @package App\repositories
 */
class FilmRepository extends Repository
{
    /**
     * @return string
     */
    public function getTableName(): string
    {
        return 'films';
    }

    /**
     * @param string|null $search
     * @param int $offset
     * @param int $limit
     * @return Film[]
     */
    public function findByTitleAndStars(?string $search, int $offset = 0, int $limit = 100): array
    {
        if (empty($search)) {
            $films = $this->findBy([], 'title ASC', $offset, $limit);
        } else {
            $search = '%' . $search . '%';
            $films = $this->findBy([
                'sql' => "title LIKE ? OR stars LIKE ?",
                'params' => [$search, $search]
            ], 'title ASC', $offset, $limit);
        }
        return array_map(function ($row) {
            $film = new Film();
            $film->setId($row['id']);
            $film->setTitle($row['title']);
            $film->setReleaseYear($row['release_year']);
            $film->setFormat($row['format']);
            $film->setStars($row['stars']);
            return $film;
        }, $films);
    }

    public function getCountByTitleAndStars(?string $search): int
    {
        if (empty($search)) {
            return $this->getCount([]);
        }
        $search = '%' . $search . '%';
        return $this->getCount([
            'sql' => "title LIKE ? OR stars LIKE ?",
            'params' => [$search, $search]
        ]);
    }
}
