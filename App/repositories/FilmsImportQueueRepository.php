<?php

namespace App\repositories;

use App\entities\FilmsImportQueue;
use Core\database\Repository;

class FilmsImportQueueRepository extends Repository
{

    public function getTableName(): string
    {
        return 'films_import_queue';
    }

    public function findBy(array $where, ?string $orderBy = null, int $offset = 0, int $limit = 0): array
    {
        return array_map(function ($data) {
            $queue = new FilmsImportQueue();
            $queue->init($data);
            return $queue;
        }, parent::findBy($where, $orderBy, $offset, $limit));
    }
}
