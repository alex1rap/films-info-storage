<?php

require_once __DIR__ . '/autoload.php';
$config = require_once __DIR__ . '/config.php';

session_start();
$app = new App($config);
$app->run();
session_write_close();
