# Документация

## Films Storage

Тестовый проект

### Установка:
Клонируем проект в локальный репозиторий:
```shell
git clone git@gitlab.com:alex1rap/films-info-storage.git
```

Ставим СУБД и PHP:
```shell
sudo add-apt-repository ppa:ondrej/php
sudo apt update
sudo apt install mariadb-server php7.3 php7.3-mysql php7.3-mbstring
```
Настраиваем СУБД, создаем пользователя, базу данных, настраиваем разрешения, переходим в директорию проекта, после чего создадим файл `db_config.php` и внесем туда настройки подключения к нашей базе данных:
```php
<?php

return [
    'driver' => 'mysql',
    'host' => 'localhost',
    'user' => 'пользователь',
    'pass' => 'пароль',
    'name' => 'имя базы данных'
];

```

Создаем 2 папки: `storage`, а в ней папку `films`, чтоб вышло `storage/films`.

Возвращаемся в корень проекта и выполняем миграции:
```shell
php bin/install.php
```

Запускаем сервер, выбрав скрипт маршрутизации:
```shell
php -S {hostname}:{port} local_webserver_router.php
```

Запускаем воркер для импорта фильмов из файла:
```shell
php bin/import_films_worker.php --worker-id 1 --batch-size 1
```
, где `--worker-id` - идентификатор воркера (если будет их несколько),
`--batch-size` - лимит очередей за одну выбоку.

## Данные для входа в админку после успешной установки:
- логин - `admin`
- пароль - `adm1nP@ss`

## Краткое описание структуры:

- ### [App](docs/app.md)
- ### [Core](docs/core.md)
- ### [Файлы конфигурации](docs/config.md)
- ### [Консольные скрипты](docs/console.md)
