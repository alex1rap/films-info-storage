<?php

use Core\base\User;
use Core\base\UserManager;
use Core\database\Connection;
use Core\database\EntityManager;
use Core\http\Request;
use Core\http\Response;
use Core\web\Router;

/**
 * Class App
 * @property Connection dbConnection
 * @property Request $request
 * @property EntityManager entityManager
 * @property User $user
 */
class App
{
    /**
     * @var array
     */
    public $config = [
        'language' => 'en_US',
        'name' => 'My Web Application'
    ];

    /**
     * @var array
     */
    protected $params = [];

    public static $app;

    public function __get($name)
    {
        return $this->params[$name];
    }

    public function __set($name, $value)
    {
        $this->params[$name] = $value;
    }

    public function __construct(array $config = [])
    {
        $this->config = array_replace_recursive($this->config, $config);
        self::$app = $this;
    }

    public function run(): void
    {
        self::$app->dbConnection = new Connection(self::$app);
        self::$app->entityManager = new EntityManager(self::$app);
        $router = new Router();
        $request = $router->parseUrl(parse_url($_SERVER['REQUEST_URI'])['path']);
        self::$app->request = $request;
        $userManager = new UserManager();
        self::$app->user = $userManager->getUser();
        $controller = $request->getController();
        $action = $request->getAction();
        $controller = new $controller();
        $result = $controller->$action();
        if ($result instanceof Response) {
            $response = $result;
        } else {
            $response = new Response();
            $response->setContent($result);
        }
        $this->sendResponse($response);
    }

    protected function sendResponse(Response $response)
    {
        $redirect = $response->getRedirect();
        foreach ($response->getHeaders() as $key => $value) {
            header($key . ': ' . $value);
        }
        foreach ($response->getCookies() as $cookie) {
            setcookie($cookie->getName(), $cookie->getValue(), $cookie->getParams());
        }
        if (!empty($redirect)) {
            header(sprintf('Location:%s', $redirect['url']), true, $redirect['code']);
            exit;
        }
        echo $response->getContent();
    }
}
