<?php

const FS_ROOT = __DIR__ . DIRECTORY_SEPARATOR . '..';

$dbConfig = require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'db_config.php';

$dsn = sprintf(
'%s:dbname=%s;host=%s',
$dbConfig['driver'],
$dbConfig['name'],
$dbConfig['host']
);
$pdo = new PDO(
$dsn,
$dbConfig['user'],
$dbConfig['pass'],
);
