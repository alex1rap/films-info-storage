<?php

require_once __DIR__ . DIRECTORY_SEPARATOR . 'console.php';

const FILES_STORAGE = FS_ROOT . '/storage/films';
const STATUS_WAITING = 0;
const STATUS_IN_PROGRESS = 1;
const STATUS_DONE = 2;
const STATUS_ERROR = -1;

if (empty($pdo)) {
    throw new Exception('DB connection was not established.');
}


/**
 * @param PDO $pdo
 * @param int $workerId
 * @param $queueId
 */
function done(PDO $pdo, int $workerId, $queueId)
{
    updateStatus($pdo, $workerId, $queueId, STATUS_DONE);
}

/**
 * @param PDO $pdo
 * @param int $workerId
 * @param $queueId
 * @param string $errorMessage
 */
function wrong(PDO $pdo, int $workerId, $queueId, string $errorMessage = 'Unknown Error')
{
    updateStatus($pdo, $workerId, $queueId, STATUS_ERROR, $errorMessage);
}

/**
 * @param PDO $pdo
 * @param int $workerId
 * @param $queueId
 * @param int $status
 * @param string|null $errorMessage
 */
function updateStatus(PDO $pdo, int $workerId, $queueId, int $status, ?string $errorMessage = null)
{
    $sql = sprintf('UPDATE films_import_queue SET status = ?, error = ?, worker_id = ? WHERE id = ?');
    $params = [$status, $errorMessage, $workerId, $queueId];
    $stmt = $pdo->prepare($sql);
    $stmt->execute($params);
}

/**
 * @param PDO $pdo
 * @param $films
 * @return bool
 */
function addFilms(PDO $pdo, $films): bool
{
    if (empty($films)) {
        return true;
    }
    $values = [];
    $params = [];
    foreach ($films as $film) {
        $values[] = '(?,?,?,?)';
        foreach ($film as $field => $value) {
            if ($field == 'stars') {
                $value = implode(',', array_unique(array_map(function ($star) {
                    return trim($star);
                }, explode(',', $value))));
            }
            $params[] = $value;
        }
    }
    $sql = "INSERT IGNORE INTO films (title, release_year, format, stars) VALUES "
        . implode(',' . PHP_EOL, $values);
    $stmt = $pdo->prepare($sql);
    return $stmt->execute($params);
}

/**
 * @param PDO $pdo
 * @param int $workerId
 * @param $queueId
 * @param $fileName
 * @return Generator
 */
function getFilmsFromFile(PDO $pdo, int $workerId, $queueId, $fileName): Generator
{
    $fields = [
        "title" => "title",
        "release year" => "release_year",
        "format" => "format",
        "stars" => "stars",
    ];
    $films = [];
    $i = 0;
    $film = [];
    foreach (getFilmsData($fileName) as $filmsData) {
        $i++;
        $error = null;
        logInfo($filmsData);
        if (empty(trim($filmsData)) && !empty($film)) {
            foreach ($fields as $field) {
                if (empty($film[$field])) {
                    $error = 'Film data is damaged';
                    logError($error);
                    break;
                }
            }
            if (null !== $error) {
                break;
            }
            logInfo("Film prepared to be inserted.\n");
            $films[] = $film;
            $film = [];
            if (count($films) >= 1000) {
                yield $films;
                $films = [];
            }
            continue;
        }
        if (preg_match('~^\s*(?<field>(Title|Release Year|Format|Stars)):(?<row>(.*))$~uisU', $filmsData, $matches)) {
            $film[$fields[mb_strtolower($matches['field'])]] = trim($matches['row']);
        } elseif (!empty($filmsData)) {
            $error = "File format is not supported or file is damaged.";
        }
        if (null !== $error) {
            logError($error);
            wrong($pdo, $workerId, $queueId, $error);
            yield ['error' => $error];
            return;
        }
    }
    yield $films;
}

/**
 * @param $fileName
 * @return Generator
 */
function getFilmsData($fileName): Generator
{
    $file = fopen($fileName, 'r');

    while (!feof($file)) {
        yield fgets($file, 1024);
    }
    fclose($file);
}

/**
 * @param string $msg
 */
function logInfo(string $msg): void
{
    writeLine('info', $msg);
}

/**
 * @param string $msg
 */
function logDebug(string $msg): void
{
    writeLine('debug', $msg);
}

/**
 * @param string $msg
 */
function logError(string $msg): void
{
    writeLine('error', $msg);
}

/**
 * @param string $level
 * @param string $msg
 */
function writeLine(string $level, string $msg): void
{
    $time = new DateTime();
    echo sprintf("%s [%s]: %s\n", $time->format('Y-m-d H:i:s'), strtoupper($level), $msg);
}

/**
 * *************************************** Import Starts Here **********************************************
 */

$options = getopt('', ['worker-id:']);
$workerId = empty($options['worker-id']) ? rand(1, 100) : (int) $options['worker-id'];
$batchSize = empty($options['batch-size']) ? 1 : (int) $options['worker-id'];
if ($batchSize < 1) {
    $batchSize = 1;
}

logInfo('Process started.');
while (true) {
    logInfo("Getting of films importing queues...");
    $sql = "SELECT * FROM films_import_queue WHERE status = ? ORDER BY id LIMIT " . $batchSize;
    $stmt = $pdo->prepare($sql);
    $stmt->execute([STATUS_WAITING]);
    $queues = $stmt->fetchAll();
    if (empty($queues)) {
        logInfo('Batch is empty. Waiting for 10 seconds to next query to DB...');
        sleep(10);
        continue;
    }
    logInfo("Got batch of importing queues from DB.\n");
    foreach ($queues as $queue) {
        logInfo(sprintf("Started processing of file %s:", $queue['file']));
        $status = STATUS_IN_PROGRESS;
        $sql = "UPDATE films_import_queue SET status = ?, worker_id = ? WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $params = [STATUS_IN_PROGRESS, $workerId, $queue['id']];
        if ($stmt->execute($params)) {
            $pdo->beginTransaction();
            logInfo(sprintf('Changed queue status to IN_PROGRESS (%d).', STATUS_IN_PROGRESS));
            foreach (getFilmsFromFile($pdo, $workerId, $queue['id'], FILES_STORAGE . '/' . $queue['file']) as $films) {
                logInfo('Read batch of films.');
                if (!empty($films['error'])) {
                    logError($films['error']);
                    $pdo->rollBack();
                    wrong($pdo, $workerId, $queue['id'], $films['error']);
                    $status = STATUS_ERROR;
                    break;
                }
                if (!addFilms($pdo, $films)) {
                    logError('Error of films insertion!');
                    $pdo->rollBack();
                    wrong($pdo, $workerId, $queue['id']);
                    $status = STATUS_ERROR;
                    break;
                }
                sleep(1);
            }
            if ($status !== STATUS_ERROR) {
                logInfo('Queue successfully processed. All films inserted to DB.');
                $pdo->commit();
                done($pdo, $workerId, $queue['id']);
            }
        } else {
            logError('Unable to change queue status!');
            wrong($pdo, $workerId, $queue['id']);
            $status = STATUS_ERROR;
        }
        loginfo('Waiting for 1 second...');
        sleep(1);
    }
    logInfo('Waiting for 10 seconds to next queues getting query...');
    sleep(10);
}
