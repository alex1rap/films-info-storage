<?php

require_once __DIR__ . DIRECTORY_SEPARATOR . 'console.php';

if (empty($pdo)) {
    throw new Exception('DB connection was not established.');
}

$dump = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'dump.sql');

$stmt = $pdo->prepare($dump);
$stmt->execute();
