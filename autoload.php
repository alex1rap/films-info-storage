<?php

define('FS_ROOT', __DIR__);

/**
 * @param $className
 * @throws Exception
 */
function loadClass($className): void
{
    $fileName = getClassFilename($className);
    if (file_exists($fileName)) {
        require_once $fileName . '';
    } else {
        throw new Exception('Not found file for class ' . $className);
    }
}

/**
 * @param $className
 * @return string
 */
function getClassFilename($className): string
{
    return str_replace(["\\", "/"], DIRECTORY_SEPARATOR, FS_ROOT . '/' . $className . '.php');
}

spl_autoload_register("loadClass");
